#ifndef SHADER_PROGRAM_HPP_
#define SHADER_PROGRAM_HPP_

#include <string>
#include <GL/gl.h>

class CompiledShader
{
public:
    CompiledShader(GLenum shaderType, std::string code);
    ~CompiledShader();
    CompiledShader(const CompiledShader &rhs) = delete;
    CompiledShader& operator=(const CompiledShader &rhs) = delete;
    GLuint id() const { return id_; }

private:
    GLenum type_;
    GLuint id_;
};

class ShaderProgram
{
public:
    ShaderProgram(const CompiledShader &vertex, const CompiledShader &fragmentProgram);
    ~ShaderProgram();
    ShaderProgram(const ShaderProgram &rhs) = delete;
    ShaderProgram& operator=(const ShaderProgram &rhs) = delete;
    ShaderProgram(ShaderProgram &&rhs);
    ShaderProgram& operator=(ShaderProgram &&rhs);
    GLuint id() const { return id_; }

private:
    GLuint id_;
};

#endif // SHADER_PROGRAM_HPP_
