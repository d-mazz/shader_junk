#ifndef GL_WINDOW_HPP_
#define GL_WINDOW_HPP_

#include <string>
#include <cstdint>
#include <SDL2/SDL.h>

class SDL_Window;

class GLWindow
{
public:
    GLWindow(std::string name, uint16_t width, uint16_t height);
    ~GLWindow();
    void resize(uint16_t width, uint16_t height);
    void present();

    int width() const { return width_; }
    int height() const { return height_; }
    bool vsync() const { return vsync_; }

private:
    SDL_Window *win_;
    SDL_GLContext context_;
    uint16_t width_;
    uint16_t height_;
    bool vsync_;
};

#endif // GL_WINDOW_HPP_
