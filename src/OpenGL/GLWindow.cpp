#include "GLWindow.hpp"
#include <stdexcept>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <GL/gl.h>
#include "util.hpp"

struct OpenGLInfo {
    const char *vendor;
    const char *renderer;
    const char *version;
    const char *shaderVersion;
    const char *extensions;
    GLint major;
    GLint minor;
};

OpenGLInfo renderInfo()
{
    OpenGLInfo opengl = {
        (const char*)glGetString(GL_VENDOR),
        (const char*)glGetString(GL_RENDERER),
        (const char*)glGetString(GL_VERSION),
        (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION),
        (const char*)glGetString(GL_EXTENSIONS)
    };
    glGetIntegerv(GL_MAJOR_VERSION, &opengl.major);
    glGetIntegerv(GL_MINOR_VERSION, &opengl.major);
    std::cout <<   "vendor: " << opengl.vendor
              << "\nrenderer: " << opengl.renderer
              << "\nversion: " << opengl.version
              << "\nmajor.minor: " << opengl.major << "." << opengl.minor
              << "\nshader: " << opengl.shaderVersion
              // << "\nextensions: " << opengl.extensions
              << std::endl;
    return opengl;
}

void initializeOpenGL(uint16_t width, uint16_t height)
{
    GL(MatrixMode, GL_PROJECTION);
    GL(LoadIdentity);
    GL(Viewport, 0, 0, width, height);
    GL(ClearColor, 0.1f, 0.1f, 0.1f, 1.0f);
}

const char *profileToCStr(SDL_GLprofile profile)
{
    const char *str = "bad_profile";
    switch(profile) {
    case SDL_GL_CONTEXT_PROFILE_CORE: str = "core"; break;
    case SDL_GL_CONTEXT_PROFILE_COMPATIBILITY: str = "compatibility"; break;
    case SDL_GL_CONTEXT_PROFILE_ES: str = "es"; break;
    default:
        break;
    }
    return str;
}

GLWindow::GLWindow(std::string name, uint16_t width, uint16_t height)
    : width_(width), height_(height)
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    win_ = SDL_CreateWindow(name.c_str(),
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            width, height,
                            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if(!win_) {
        fprintf(stderr, "Failed to create window\n");
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
        SDL_Quit();
        throw std::runtime_error("Failed to create window");
    }
    context_ = SDL_GL_CreateContext(win_);
    if(!context_) {
        fprintf(stderr, "Failed to create OpenGL context\n");
        SDL_DestroyWindow(win_);
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
        SDL_Quit();
        throw std::runtime_error("Failed to create OpenGL context");
    }
    int tmp[3];
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &tmp[0]);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &tmp[1]);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &tmp[2]);
    printf("OpenGL %s profile %i.%i\n", profileToCStr(static_cast<SDL_GLprofile>(tmp[0])), tmp[1], tmp[2]);
    SDL_GL_GetAttribute(SDL_GL_DOUBLEBUFFER, &tmp[0]);
    printf("Double buffering is %s\n", tmp ? "on" : "off");

    // Set vsync on
    vsync_ = true;
    if(SDL_GL_SetSwapInterval(1)) {
        std::cerr << "Warning: vsync not supported\n";
        vsync_ = false;
    }
    renderInfo();
    initializeOpenGL(width_, height_);
}

GLWindow::~GLWindow()
{
    SDL_GL_DeleteContext(context_);
    SDL_DestroyWindow(win_);
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
    SDL_Quit();
}

void GLWindow::resize(uint16_t width, uint16_t height)
{
    width_ = width;
    height_ = height;
    initializeOpenGL(width, height);
}

void GLWindow::present()
{
    SDL_GL_SwapWindow(win_);
}
