#include "util.hpp"
#include <GL/gl.h>
#include <iostream>

const char *glErrorToCString(GLenum err)
{
    const char *msg = "what?";
    switch(err) {
    case GL_INVALID_ENUM: msg = "invalid enum"; break;
    case GL_INVALID_VALUE: msg = "invalid value"; break;
    case GL_INVALID_OPERATION: msg = "invalid operation"; break;
    case GL_STACK_OVERFLOW: msg = "stack overflow"; break;
    case GL_STACK_UNDERFLOW: msg = "stack underflow"; break;
    case GL_OUT_OF_MEMORY: msg = "out of memory"; break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:  msg = "invalid framebuffer operation"; break;
    case GL_CONTEXT_LOST: msg = "context lost"; break;
    case GL_TABLE_TOO_LARGE: msg = "table too large"; break;
    default:
        break;
    }
    return msg;
}

void glCheckErrorHelper(const char *file, const int line, const char *note)
{
    GLenum err = glGetError();
    if(!err) {
        return;
    }
    const char *errstr = glErrorToCString(err);
    if(note) {
        std::cerr << file << ":" << line << ": OpenGL error (" << err << ") " << errstr << " - " << note << std::endl;
    }
    else {
        std::cerr << file << ":" << line << ": OpenGL error (" << err << ") " << errstr << std::endl;
    }
}
