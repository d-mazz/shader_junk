#include "ShaderProgram.hpp"
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <GL/gl.h>
#include <GL/glext.h>
#include "util.hpp"
using namespace std;

typedef void (*GetShaderLogVal)(GLuint id, GLenum pname, GLint *params);
typedef void (*GetShaderInfoLog)(GLuint id, GLsizei maxLength, GLsizei *length, GLchar *infoLog);

std::string getShaderLogGen(GLuint id, GetShaderLogVal logvalfunc, GetShaderInfoLog infologfunc)
{
    GLint logSize;
    std::string log;
    (*logvalfunc)(id, GL_INFO_LOG_LENGTH, &logSize);
    if(logSize) {
        GLchar *clog = new GLchar[logSize+1];
        memset(clog, 0, logSize+1);
        GLsizei len;
        (*infologfunc)(id, logSize, &len, clog);
        log.assign(clog);
        delete[] clog;
    }
    return log;
}

void compileShader(GLenum shaderType, GLuint shaderId, const char *shaderCode)
{
    using namespace std;
    GLint compileSuccess = GL_FALSE;
    GL(ShaderSource, shaderId, 1, &shaderCode, nullptr);
    GL(CompileShader, shaderId);
    GL(GetShaderiv, shaderId, GL_COMPILE_STATUS, &compileSuccess);
    if(compileSuccess != GL_TRUE) {
        std::string log = getShaderLogGen(shaderId, glGetShaderiv, glGetShaderInfoLog);
        std::string errorMsg = std::string("Failed to compile shader: \"") + log + "\"";
        GL(DeleteShader, shaderId);
        throw std::runtime_error(errorMsg);
    }
}

void linkProgram(GLuint programId, const CompiledShader &vertex, const CompiledShader &fragment)
{
    GLint linkSuccess = GL_FALSE;
    GL(AttachShader, programId, vertex.id());
    GL(AttachShader, programId, fragment.id());
    GL(LinkProgram, programId);
    GL(UseProgram, programId);

    GL(GetProgramiv, programId, GL_LINK_STATUS, &linkSuccess);
    if(!linkSuccess) {
        std::string log = getShaderLogGen(programId, glGetProgramiv, glGetProgramInfoLog);
        std::string errorMsg = std::string("Failed to compile shader: \"") + log + "\"";
        GL(DeleteProgram, programId);
        throw std::runtime_error(errorMsg);
    }
}

CompiledShader::CompiledShader(GLenum shaderType, std::string code)
    : type_(shaderType)
{
    id_ = glCreateShader(type_);
    compileShader(shaderType, id_, code.c_str());
}

CompiledShader::~CompiledShader()
{
    GL(DeleteShader, id_);
}

ShaderProgram::ShaderProgram(CompiledShader const &vertex, CompiledShader const &fragment)
    : id_(0)
{
    id_ = glCreateProgram();
    linkProgram(id_, vertex, fragment);
}

ShaderProgram::~ShaderProgram()
{
    if(id_)
        GL(DeleteProgram, id_);
}

ShaderProgram::ShaderProgram(ShaderProgram &&rhs)
{
    id_ = rhs.id_;
    rhs.id_ = 0;
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram &&rhs)
{
    id_ = rhs.id_;
    rhs.id_ = 0;
    return *this;
}
