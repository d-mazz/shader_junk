#ifndef OPENGL_UTIL_HPP_
#define OPENGL_UTIL_HPP_

#include <utility>

void glCheckErrorHelper(const char *file, const int line, const char *note = nullptr);

/**
 * A better solution might be to programmatically create a header
 * containing all of the OpenGL functions inside of a debug/glcheck namespace.
 * Though this would have the limitation of losing the file/line number.
 */
template<typename Func_t, typename... Args>
void glCheckError(const char *file, int line, Func_t func, Args&&... args)
{
    func(std::forward<Args>(args)...);
    glCheckErrorHelper(file, line);
}

#define GL(FUNCTION, ...) \
    glCheckError(__FILE__, __LINE__, gl##FUNCTION,  ##__VA_ARGS__)

#endif // OPENGL_UTIL_HPP_
