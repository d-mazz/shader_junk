#version 330 core
layout (location = 0) out vec4 fragColor;

const float kEpsilon = 0.01;
const int kMaxSteps = 32;
const vec3 lightSrc = vec3(10.0, 10.0, 10.0);
const float pi = 3.1415926535;

float distSphere(vec3 rayPos, vec3 spherePos, float radius)
{
    return length(spherePos - rayPos) - radius;
}

float sceneDistance(vec3 p)
{
    return distSphere(p, vec3(0.5, 0, 1), 1.5);
}

vec3 computeNormal(vec3 pos, float dist)
{
    float right = sceneDistance(pos + vec3(kEpsilon, 0, 0)) - dist;
    float up = sceneDistance(pos + vec3(0, kEpsilon, 0)) - dist;
    float forward = sceneDistance(pos + vec3(0, 0, kEpsilon)) - dist;
    return normalize(vec3(right, up, forward));
}

vec4 rayMarch(vec3 ro, vec3 rd)
{
    float t = 0;
    for(int i=0; i < kMaxSteps; ++i) {
        vec3 p = ro + t * rd;
        float d = sceneDistance(p);
        if(d < kEpsilon) {
            vec3 norm = computeNormal(p, d);
            return vec4(-dot(norm, rd));
        }
        t += d;
    }
    return vec4(0);
}

void main()
{
    float eyeDist = 0.5;
    vec3 eye = vec3(0, 0, -eyeDist);
    vec3 up = vec3(0, 1, 0);
    vec3 right = vec3(1, 0, 0);

    float ratio = 640.0 / 480.0;
    float u = gl_FragCoord.x * 2.0 / 640 * ratio - 1.0;
    float v = gl_FragCoord.y * 2.0 / 480 - 1.0;
    vec3 forward = -eye;
    vec3 ro = eye + right * u + up * v;
    vec3 rd = normalize(cross(right,up) + (ro - eye));
    fragColor = rayMarch(ro, rd);
}
