#version 420 core

in vec2 uv;
out vec4 color;

layout (binding=0) uniform sampler2D tex0;
layout (binding=1) uniform sampler2D tex1;

void main() {
    if(uv.x > 0.5) {
        color = texture(tex1, uv.xy).rgba;
    }
    else {
        color.rg = uv.xy;
        color.b = texture(tex0, uv.xy).b;
        color.a = texture(tex0, uv.xy).a;
    }
}
