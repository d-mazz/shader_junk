#version 330 core

in vec3 pos;
out vec2 uv;

void main()
{
    gl_Position = vec4(pos.x, pos.y, pos.z, 1.1);
    uv.xy = (pos.xy + 1.0) / 2.0;
}
