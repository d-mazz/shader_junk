#include "DirectoryWatcher.hpp"
#include <unistd.h>
#include <sys/inotify.h>
#include <stdexcept>

DirectoryWatcher::DirectoryWatcher(const char *path)
{
    fd_.resize(2);
    fd_[0] = inotify_init1(IN_NONBLOCK | IN_CLOEXEC);
    if(fd_[0] < 0) {
        throw std::runtime_error("Failed to create inotify watch");
    }
    fd_[1] = inotify_add_watch(fd_[0], path, (IN_CREATE | IN_DELETE | IN_MODIFY));
    if(fd_[1] < 0) {
        close(fd_[0]);
        throw std::runtime_error("Failed to create inotify watch");
    }
}

DirectoryWatcher::~DirectoryWatcher()
{
    close(fd_[1]);
    close(fd_[0]);
}

int DirectoryWatcher::changed()
{
    int numEvents = 0;
    char buf[4096];
    ssize_t len = read(fd_[0], buf, sizeof(buf));
    if(len >= 0) {
        const struct inotify_event *event = reinterpret_cast<const inotify_event*>(buf);
        if(event->mask & (IN_CREATE | IN_DELETE | IN_MODIFY)) {
            ++numEvents;
        }
    }
    return numEvents;
}
