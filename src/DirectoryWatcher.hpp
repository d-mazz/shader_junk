#ifndef DIRECTORY_WATCHER_HPP_
#define DIRECTORY_WATCHER_HPP_

#include <vector>

class DirectoryWatcher
{
public:
    DirectoryWatcher(const char *path);
    ~DirectoryWatcher();
    
    int changed();

private:
    std::vector<int> fd_;
};

#endif // DIRECTORY_WATCHER_HPP_
