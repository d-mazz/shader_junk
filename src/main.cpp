#include <fstream>
#include <iostream>
#include <ctime>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include "DirectoryWatcher.hpp"
#include "OpenGL/GLWindow.hpp"
#include "OpenGL/ShaderProgram.hpp"
#include "OpenGL/util.hpp"

static const std::string kShaderRoot = SHADER_DIR;
size_t constexpr kNumComp = 4; // Number of color components

void handleWindowEvents(GLWindow *win, const SDL_Event &event)
{
    switch(event.window.event) {
    case SDL_WINDOWEVENT_RESIZED:
        win->resize(event.window.data1, event.window.data2);
        break;
    default:
        break;
    }
}

/**
 * Texture-backed Framebuffer in OpenGL nomenclature. Used for intermediate image rendering that the CPU nay want to read pixel data from.
 */
class FBTexture {
    size_t width_;
    size_t height_;
    GLuint texId_;
    GLuint fbId_;
    bool active_;

public:
    FBTexture(size_t width, size_t height)
        : width_(width), height_(height), texId_(0), fbId_(0), active_(false)
    {
        GL(GenFramebuffers, 1, &fbId_);
        GL(BindFramebuffer, GL_FRAMEBUFFER, fbId_);
        GL(GenTextures, 1, &texId_);
        GL(BindTexture, GL_TEXTURE_2D, texId_);
        GL(TexParameteri, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        GL(TexParameteri, GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        GL(Viewport, 0, 0, width_, height_);
        GL(TexImage2D, GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
        GL(FramebufferTexture2D, GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId_, 0);
    }
    ~FBTexture()
    {
        if(active_) {
            fprintf(stderr, "Warning, no infrastructure to set back to last framebuffer\n");
            // GL(DrawBuffer, GL_BACK);
        }
        GL(DeleteTextures, 1, &texId_);
        GL(DeleteFramebuffers, 1, &fbId_);
    }
    FBTexture(FBTexture &&rhs)
        : width_(rhs.width_), height_(rhs.height_), texId_(rhs.texId_), fbId_(rhs.fbId_), active_(rhs.active_)
    {
        rhs.texId_ = 0;
        rhs.fbId_ = 0;
    }
    FBTexture(const FBTexture &) = delete;
    FBTexture operator=(const FBTexture &) = delete;
    void makeActive()
    {
        GL(BindFramebuffer, GL_FRAMEBUFFER, fbId_);
        GL(Viewport, 0, 0, width_, height_);
//         GLenum drawbufs[1] = {GL_COLOR_ATTACHMENT0};
//         GL(DrawBuffers, 1, drawbufs);
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if(status != GL_FRAMEBUFFER_COMPLETE)
            fprintf(stderr, "CheckFramebufferStatus returned %i\n", status);
    }
    GLuint texId() { return texId_; }
    GLuint fbId() { return fbId_; }
    size_t width() { return width_; }
    size_t height() { return height_; }
};

struct RenderStages {
    // There is expected to be one more program than FBTextures, the final Frame buffer is
    // the default screen framebuffer.
    std::vector<ShaderProgram> programs;
    std::vector<FBTexture> framebufs;
};

void writePPMFile(const std::string &filename, const char *pixels, size_t width, size_t height)
{
    FILE *fp = fopen(filename.c_str(), "we");
    fprintf(fp, "P6 %zu %zu 255 ", width, height);
    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            fwrite(&pixels[kNumComp * (y * width + x)], 3, 1, fp);
        }
    }
    fclose(fp);
}

void updateWindow(GLWindow *win, RenderStages &stages, bool dumpStages)
{
    GL(ClearColor, 0.0f, 0.4f, 0.5f, 0.0f);
    GL(Clear, (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
    GLuint vb, ib;
    GL(GenBuffers, 1, &vb);
    GL(BindBuffer, GL_ARRAY_BUFFER, vb);
    GL(GenBuffers, 1, &ib);
    GL(BindBuffer, GL_ELEMENT_ARRAY_BUFFER, ib);
    constexpr size_t kNumVerts = 4;
    constexpr size_t kNumIndices = 6;
    glm::vec3 verts[kNumVerts] = {
        {-1.0, -1.0, 1.0},
        {-1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0},
        {1.0, -1.0, 1.0}
    };
    GLuint indices[kNumIndices] = {
        0, 3, 1, 1, 3, 2
    };
    GL(EnableVertexAttribArray, 0);
    GL(VertexAttribPointer, 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    GL(BufferData, GL_ARRAY_BUFFER, kNumVerts * sizeof(glm::vec3), &verts[0], GL_STATIC_DRAW);
    GL(BufferData, GL_ELEMENT_ARRAY_BUFFER, kNumIndices * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);

    // Draw the first n-1 buffers to a texture, and make available to subsequent stages
    std::vector<char> pixels;
    int i;
    for(i = 0; i < stages.framebufs.size(); ++i) {
        stages.framebufs[i].makeActive();
        GL(UseProgram, stages.programs[i].id());
        GL(DrawElements, GL_TRIANGLES, kNumIndices, GL_UNSIGNED_INT, nullptr);
        GL(ActiveTexture, GL_TEXTURE0+i);
        GL(BindTexture, GL_TEXTURE_2D, stages.framebufs[i].texId());

        if(!dumpStages)
            continue;

        // Test code to write the intermediate buffers to disk
        pixels.resize(stages.framebufs[i].width() * stages.framebufs[i].height() * kNumComp);
        GL(GetTexImage, GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
        std::string filename = std::string("stage_") + std::to_string(i) + "_" +
            std::to_string(stages.framebufs[i].width()) + "x" + std::to_string(stages.framebufs[i].height()) +
            ".ppm";
        writePPMFile(filename, pixels.data(), stages.framebufs[i].width(), stages.framebufs[i].height());
    }
    // Draw the last stage
    GL(BindFramebuffer, GL_FRAMEBUFFER, 0);
    GL(UseProgram, stages.programs[i].id());
    GL(DrawElements, GL_TRIANGLES, kNumIndices, GL_UNSIGNED_INT, nullptr);

    GL(DeleteBuffers, 1, &ib);
    GL(DeleteBuffers, 1, &vb);
    win->present();
}

std::string readFile(const char *path)
{
    using namespace std;
    string body;
    std::cout << "reading path \"" << path << "\"" << std::endl;
    ifstream shaderStream(path, ios::in);
    if(shaderStream.is_open()) {
        string line;
        while(getline(shaderStream, line)) {
            body += "\n" + line;
        }
        shaderStream.close();
    }
    return body;
}

int addShaders(size_t numShaders, std::vector<ShaderProgram> *shaders)
{
    using namespace std;
    string tmp;
    string filename;
    string vertexCode;
    string fragmentCode;
    try {
        for(size_t i=0; i < numShaders; ++i) {
            filename = kShaderRoot + "/junk" + to_string(i) + ".vert";
            vertexCode = readFile(filename.c_str());
            filename = kShaderRoot + "/junk" + to_string(i) + ".frag";
            fragmentCode = readFile(filename.c_str());
            CompiledShader vertex(GL_VERTEX_SHADER, vertexCode);
            CompiledShader fragment(GL_FRAGMENT_SHADER, fragmentCode);
            shaders->emplace_back(vertex, fragment);
        }
    }
    catch(exception &e) {
        cerr << e.what() << endl;
    }
    return 0;
}

void buildStages(const GLWindow &win, int numStages, RenderStages *stages)
{
    stages->programs.clear();
    stages->framebufs.clear();
    addShaders(numStages, &stages->programs);
    if(stages->programs.empty())
        return;
    for(int i=0; i < stages->programs.size()-1; ++i) {
        stages->framebufs.emplace_back(win.width(), win.height());
    }
}

int main(int argc, char *argv[])
{
    GLWindow win("shaderToy", 640, 480);
    SDL_Event event;
    // We just watch the directory for changes
    DirectoryWatcher watcher(kShaderRoot.c_str());
    bool running = true;
    RenderStages stages;
    constexpr int kNumStages = 3;
    buildStages(win, kNumStages, &stages);
    bool didDump = false;
    while(running) {
        updateWindow(&win, stages, !didDump);
        didDump = true;
        if(watcher.changed()) {
            didDump = false;
            buildStages(win, kNumStages, &stages);
        }
        if(!win.vsync()) {
            SDL_Delay(16);
        }
        if(!SDL_PollEvent(&event)) {
            continue;
        }
        switch(event.type) {
        case SDL_WINDOWEVENT:
            handleWindowEvents(&win, event);
            break;
        case SDL_QUIT:
            running = false;
            break;
        }
    }
    return 0;
}
